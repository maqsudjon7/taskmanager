$(document).ready(function () {
  //runs when user clicks edit button
  $('body').on('click', '.icon-edit', function () {
    let input = $(this).parent().parent().find('input');
    input.removeAttr('disabled').focus(); //Removing disabled attr to be able to edit title
    input.parent().addClass('on-focus');
  });

  //runs when user finishes editing, and sends POST request to server for updating title 
  $(document).on('blur', '.task-title > input', function () {
    let title = $(this).val();  //modified title
    let input = $(this);
    if (title.length > 50) {  //validating length
      input.parent().addClass('has-error').find('.form-validation').text('Length can not exeed 50 character!');
      input.focus();
      return;
    }

    let id = input.data('item-id'); //taking id of element
    $.ajax({
      type: "POST",
      url: `/api/update/${id}`,
      data: {
        id,
        title
      },
      success: function () {
        input.parent().removeClass('has-error on-focus');
      },
      error: function (e) {
        e = $.parseJSON(e.responseText);
        $('.description-label').addClass('has-error').find('.form-validation').text(e.title);
        input.parent().addClass('has-error').find('.form-validation').text(e.responseText);
        input.focus();
        return;
      }
    });



    $(this).attr('disabled', 'disabled');
  });

  //runs when user clicks to title, and sends GET request to server and presents results in modal to a user
  $('body').on('click', '.task-title', function () {
    if ($(this).hasClass('on-focus')) return;

    let id = $(this).find('input').data('item-id');
    $.ajax({
      type: "GET",
      url: `/api/viewTask/${id}`,
      success: function (data) {
        data = $.parseJSON(data);
        $('#modal-title').text(data.title);
        $('#modal-meta').html(`<p>Task status: ${data.status}<p>`);
        $('#modal-meta').append(`<p>Task was created by: ${data.user_name}<p>`);
        if (data.is_updated) {
          $('#modal-meta').append(`<p>Task was updated by user<p>`);
        }

        $("#desc").val(`${data.description == 'null' ? '' : data.description}`).data('item-id', data.id);
        $('#exampleModal').modal('toggle');
      }
    });
  });

  //loading more tasks
  $('body').on('click', '.show-more', function () {
    if ($(this).hasClass('on-focus')) return;

    let items = $('.task-item').length - 2;
    $.ajax({
      type: "GET",
      url: `/api/showMore/${items}`,
      success: function (data, textStatus, xhr) {
        if (xhr.status === 204) {
          $('.task-item').last().remove();
          return;
        }
        data = $.parseJSON(data);
        $.each(data, function (index, value) {
          $(`.task-list .task-item:nth-child(${items + 1})`).after(`
              <div class="task-item">
                  <div class="task-check-box">
                      <label class="checkbox-label">
                          <input ${value.status == 'done' ? 'checked' : ''} data-item-id="${value.id}" type="checkbox" name="" id="">
                          <span class="checkbox-custom"></span>
                      </label>
                  </div>
                  <div class="task-content">
                      <div class="task-title">
                          <input data-item-id="${value.id}" type="text" disabled='disabled' name="title" value="${value.title}" id="">
                          <span class="form-validation"></span>
                      </div>
                      <div class="actions">
                          <span class="icon-edit list-card-operation"></span>
                          <span data-item-id="${value.id}" class="delete-icon">
                          </span>
                      </div>
                  </div>
            `);
        })
      }
    });
  });

  //removes att disabled when user clicks on textarea
  $('body').on('click', '.description-label', function () {
    let textarea = $(this).find('textarea');
    textarea.removeAttr('disabled').focus();
  });

  //adds att disabled after focusing on element
  $('body').on('blur', '.description-label textarea', function () {
    $(this).attr('disabled', 'disabled');
  });

  //runs when user clicks to save button, and sends POST request to server for description modification
  $('#save').on('click', function () {
    let textarea = $('#desc');
    let description = textarea.val();
    if (description.length > 255) {
      $('.description-label').addClass('has-error').find('.form-validation').text('Length can not exeed 255 character!');
      textarea.focus();
      return;
    }
    let id = textarea.data('item-id');
    $.ajax({
      type: "POST",
      url: `/api/update/${id}`,
      data: {
        id,
        description
      },
      success: function () {
        textarea.parent().removeClass('has-error');
        $(textarea).attr('disabled', 'disabled');
      },
      error: function (e) {
        e = $.parseJSON(e.responseText);
        $('.description-label').addClass('has-error').find('.form-validation').text(e.description);
        textarea.focus();
        return;
      }
    });
  });

  //runs when user changes the status, and sends POST request to server for status modification
  $('body').on('click', '.checkbox-custom', function () {
    let id = $(this).prev().data('item-id');
    $.ajax({
      type: "POST",
      url: `/api/updateStatus/${id}`,
      success: function () {
      },
      error: function (e) {
        alert(e);
      }
    });
  });

  //runs when user finishes writing title, and sends POST request to server for task creation 
  $(document).on('blur', '.create-task', function () {
    let input = $(this);
    let title = input.val();
    if (title.length > 50) {
      input.parent().addClass('has-error').find('.form-validation').text('Length can not exeed 50 character!');
      input.focus();
      return;
    } else if (title.length === 0) {
      input.parent().addClass('has-error').find('.form-validation').text('Title can not be empty!');
      input.focus();
      return;
    }
    $.ajax({
      type: "POST",
      url: `/api/create`,
      data: {
        title
      },
      success: function (item) {
        input.parent().parent().after(` 
          <div class="task-item">
              <div class="task-check-box">
                  <label class="checkbox-label">
                      <input data-item-id="${item}" type="checkbox" name="" id="">
                      <span class="checkbox-custom"></span>
                  </label>
              </div>
              <div class="task-content">
                  <div class="task-title">
                      <input data-item-id="${item}" type="text" disabled='disabled' name="title" value="${title}" id="">
                      <span class="form-validation"></span>
                  </div>
                  <div class="actions">
                      <span class="icon-edit list-card-operation"></span>
                      <span data-item-id="${item}" class="delete-icon">
                      </span>
                  </div>
              </div>
          </div>
          `);
        $('.task-item:nth-child(7)').remove();
        input.val('');
        input.parent().removeClass('has-error');
      },
      error: function (e) {
        input.parent().addClass('has-error').find('.form-validation').text(e.responseText);
      }
    });



  });

  //runs when user clicks delete icon, and sends POST request to server for task deletion 
  $('body').on('click', '.delete-icon', function () {
    let id = $(this).data('item-id');
    let element = $(this);
    $.ajax({
      type: "POST",
      url: `/api/delete/${id}`,
      success: function () {
        element.parent().parent().parent().remove();
      }
    });
  });
});