<?php

session_start();

require 'vendor/autoload.php';

require 'application/database.php';

use Application\Core\App;

$app = new App();
