<?php

namespace Application\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public static $validationRule = [
        'login'    => ['required' => true],
        'password' => ['maxLength' => 255, 'required' => true]
    ];
}
