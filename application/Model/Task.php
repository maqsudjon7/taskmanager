<?php

namespace Application\Model;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['title','description', 'user_id', 'created_at', 'updated_at', 'is_updated'];

    public static $validationRuleApi = [
        'title'   => ['maxLength' => 50],
        'description'   => ['maxLength' => 255],
    ];
}
