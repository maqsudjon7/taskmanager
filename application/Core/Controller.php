<?php

namespace Application\Core;


class Controller
{
    /**
     * Validates the given data by rule.
     * 
     * @param array rule of validation $rule
     * @return bool
     */
    public function validate($rule)
    {
        if ($_SESSION['validationErrors']) {
            unset($_SESSION['validationErrors']);
        }
        foreach ($rule as $key => $value) {
            foreach ($value as $key2 => $value2) {
                switch ($key2) {
                    case 'required':
                        if (empty($_POST[$key])) {
                            $_SESSION['validationErrors'] = [$key => "Обязательное поле"];
                            return false;
                        }
                        break;
                    case 'email':
                        if (!filter_var($_POST[$key], FILTER_VALIDATE_EMAIL) && isset($_POST[$key])) {
                            $_SESSION['validationErrors'] = [$key => "Неверный формат почты. Пример: test@gmail.com."];
                            return false;
                        }
                        break;
                    case 'maxLength':
                        if (mb_strlen($_POST[$key]) > $value2) {
                            $_SESSION['validationErrors'] = [$key => "Max. length of title should not be grater than: {$value2}"];
                            return false;
                        }
                }
            }
        }
        return true;
    }

    /**
     * Display specified page.
     * 
     * @param string the view name $viewName
     * @param array data which should be passed to page $data
     */
    public function view($viewName, $data = null)
    {
        require_once 'application/View/' . $viewName . '.php';
    }

    /**
     * Redirects to specified url.
     * 
     * @param string the url where a user should be directed to $url
     * @return Application\Core\App.
     */
    public function redirect($url)
    {
        return App::redirect($url);
    }

    public function clearHTLMTags($text)
    {
        return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
    }
}
