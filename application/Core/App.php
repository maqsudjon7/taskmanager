<?php

namespace Application\Core;

class App
{
    /**
     * Default controller.
     */
    protected $controller = [
        'LoginController', 'ApiController'
    ];

    /**
     * Default method.
     */
    protected $method = 'index';

    /**
     * Default params.
     */
    protected $params = [];

    /**
     * List of the action which can be accessed only by POST request.
     */
    protected $postRequests = [
        'update', 'create', 'updateStatus',
        'logOut', 'delete'
    ];

    /**
     * List of the action which can be accessed only by Admin.
     */
    protected $adminRoutes = [
        'update', 'updateStatus', 'updateIndex',
        'logOut', 'delete'
    ];

    public function __construct()
    {
        list($controller, $method, $params[]) = $this->getUrl();
        $controller = ucfirst($controller) . "Controller";
        if (in_array($controller, $this->controller)) {
            $this->controller = "Application\Controller\\" . $controller;
        } else {
            $this->controller = "Application\Controller\ApiController";
        }
        $this->controller = new $this->controller;

        if ($this->validateMethod($method)) {

            $this->method = $method;
        }

        $this->params = $params ?? [];

        call_user_func_array(
            [
                $this->controller,
                $this->method
            ],
            $this->params
        );
    }

    /**
     * Prepares array, which includes action, method and params given by user.
     * 
     * @param string the url which is requested by user $url
     * @return array of action/method/...params.
     */
    public function getUrl($url = null)
    {
        if (isset($_GET['url'])) {
            $url = rtrim($_GET['url'], '/');
            return explode(
                '/',
                filter_var($url, FILTER_SANITIZE_URL)
            );
        }
    }

    /**
     * Checks whether controller exist or not.
     * 
     * @param string controller name which should be checked $contollerName
     * @return bool.
     */
    public static function controllerExist($contollerName)
    {
        return file_exists("../application/Controller/{$contollerName}.php");
    }

    /**
     * Redirects to specified url.
     * 
     * @param string the url where a user should be directed to $url
     * @return bool.
     */
    public static function redirect($url)
    {
        return header("Location: /$url/");
    }

    /**
     * Checks whether specified route/method is awailable only for admin or for everyone.
     * 
     * @param string route which should be checked $route
     * @return bool.
     */
    public function isAuthRoute($route)
    {
        return in_array($route, $this->adminRoutes);
    }

    public function isAuthorized()
    {
        return isset($_SESSION['user']);
    }

    public function validateRequestMethod($method)
    {
        return in_array($method, $this->postRequests);
    }

    /**
     * Checks the permition of users and whether specified method of request is valid.
     * 
     * @param string 
     * @return bool.
     */
    public function validateMethod($method)
    {
        $requestMethod = $_SERVER['REQUEST_METHOD'];

        if ($this->isAuthRoute($method) && !$this->isAuthorized()) {
            return self::redirect('main/index');
        }

        if ($requestMethod !== "POST" && $this->validateRequestMethod($method)) {
            return self::redirect('main/index');
        }

        return method_exists($this->controller, $method);
    }
}
