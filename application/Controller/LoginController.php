<?php

namespace Application\Controller;

use Application\Core\Controller;
use Application\Model\User;

class LoginController extends Controller
{
    /**
     * Veryfy signed user.
     *
     * @return \Application\Core\Controller
     */
    public function login()
    {
        $validate = $this->validate(User::$validationRule);

        if (!$validate) {
            return $this->view('index');
        }

        $login = $_POST['login'];
        $password = $_POST['password'];

        $user = User::where("login", $login)->first();
        if (
            $user &&
            password_verify($password, $user->password)
        ) {
            $_SESSION['user'] = $user;
            return $this->redirect('main/index');
        }

        $_SESSION['errorMessage'] = "Wrong username or password entered!";
        return $this->redirect('api/index');
    }

    /**
     * Logout action. Clears all session variables
     *
     * @return \Application\Core\Controller
     */
    public function logOut()
    {
        session_unset();
        return $this->redirect('api/index');
    }
}
