<?php

namespace Application\Controller;

use Application\Model\Task;
use Application\Core\Controller;

class ApiController extends Controller
{
    /**
     * Display a page of the tasks.
     *
     * @return \Application\Core\Controller
     */
    public function index()
    {
        return $this->view(
            'index', 
            Task::orderBy('created_at', 'desc')
                ->take('5')
                ->get()
        );
    }

    /**
     * Store a created resource in database.
     *
     * @return \Application\Core\Controller
     */
    public function create()
    {
        $validate = $this->validate(Task::$validationRuleApi);

        $data = $_POST;
        $data['status'] = "not done";
        if (isset($data['description'])) {
            $data['description']   = $this->clearHTLMTags($data['description']);
        }
        if (!$validate) {
            header("HTTP/1.1 500 Internal Server Error");
            echo($_SESSION['validationErrors']['title']);
            die();
        }
        $user = unserialize(serialize($_SESSION['user']));
        $data['user_id'] = $user->id;

        $data = Task::create($data);
        if ( $data ) {
            header("HTTP/1.1 200 OK");
            echo $data->id;
        } else {
            header("HTTP/1.1 500 Internal Server Error");
            echo '500 Internal Server Error';
        };
        
    }

    /**
     * Update the specified task in database.
     *
     * @param  int  $id
     * @return \Application\Core\Controller
     */
    public function update($id)
    {
        $validate = $this->validate(Task::$validationRuleApi);

        $data = $_POST;
        if (isset($data['text'])) {
            $data['text']   = $this->clearHTLMTags($data['text']);
        }
        $data['is_updated'] = true;
        $data['email']  = $data['email'] ?? 'guest@gmail.com';
        if (!$validate) {
            header("HTTP/1.1 500 Internal Server Error");
            echo json_encode($_SESSION['validationErrors']);
            die();
        }
        $task = Task::find($id);
        if ( $task->update($data) ) {
            header("HTTP/1.1 200 OK");
            echo 'success';
        } else {
            header("HTTP/1.1 500 Internal Server Error");
            echo '500 Internal Server Error';
        };
    }

    /**
     * Update the status of specified task in database.
     *
     * @param  int  $id
     * @return \Application\Core\Controller
     */
    public function updateStatus($id)
    {
        if (!isset($_SESSION['user'])) {
            return $this->redirect('main/index');
        }
        $task = Task::find($id);

        if ($task->status == 'done') {
            $task->status = "not done";
        } else {
            $task->status = "done";
        }
        
        if ( $task->update() ) {
            header("HTTP/1.1 200 OK");
            echo "status was updated";
        } else {
            header("HTTP/1.1 500 Internal Server Error");
            echo '500 Internal Server Error';
        };
    }

    public function viewTask($id)
    {
        $task = Task::select(['tasks.id', "tasks.title", 'tasks.description', 'tasks.status', 'tasks.is_updated', 'users.login as user_name'])
                      ->join('users', 'tasks.user_id', '=','users.id')
                      ->where('tasks.id', $id)
                      ->first();
        if ( $task ) {
            header("HTTP/1.1 200 OK");
            echo json_encode($task, true);
        } else {
            header("HTTP/1.1 204 No content");
            echo '204 No content';
        };
    }


    /**
     * Retreive more data from DB, by default it is 5
     *
     * @return string json format of tasks
     */
    public function showMore(int $skip)
    {
        if (!is_numeric($skip)) {
            header("HTTP/1.1 500 Internal Server Error");
            die();
        }
        $task = Task::select(['id', "title", 'description', 'status'])
            ->skip($skip)
            ->take(5)
            ->get();
            
        if ( count($task) > 0 ) {
            header("HTTP/1.1 200 OK");
            echo json_encode($task);
        } else {
            header("HTTP/1.1 204 No content");
            echo '204';
        };
    }

    /**
     * Delete the specified task from database.
     *
     * @param  int  $id
     * @return \Application\Core\Controller
     */
    public function delete($id)
    {
        if (!isset($_SESSION['user'])) {
            return $this->redirect('main/index');
        }

        $task = Task::find($id);
        if ( $task->delete() ) {
            header("HTTP/1.1 200 OK");
            echo 'success';
        } else {
            header("HTTP/1.1 500 Internal Server Error");
            echo '500 Internal Server Error';
        };
    }
}
