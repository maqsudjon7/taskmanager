<?php
$user = unserialize(serialize($_SESSION['user']));
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Task manager</title>
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Task manager</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
      </ul>
      <?php if ($user) : ?>
        <span class="navbar-text">
          Hello! <?= $user->login ?>,
          <a href="" onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();">
            Logout </a>

          <form id="logout-form" action="/login/logOut" method="POST" style="display: none"></form>
        </span>
      <?php endif; ?>
    </div>
  </nav>
  <?php if ($user) : ?>
    <section class="content">
      <div class="task-list">
        <div class="task-item task-create">
          <div class="task-check-box">
            <label class="checkbox-label">
              <input disabled type="checkbox" name="" id="">
              <span class="checkbox-custom"></span>
            </label>
          </div>
          <div class="task-content">
            <input type="text" class="create-task" name="title" placeholder="Enter new task..." id="">
            <span class="form-validation"></span>
          </div>
        </div>
        <?php foreach ($data as $value) : ?>
          <div class="task-item">
            <div class="task-check-box">
              <label class="checkbox-label">
                <input data-item-id="<?= $value['id'] ?>" <?= $value['status'] == 'done' ? 'checked' : '' ?> type="checkbox" name="" id="">
                <span class="checkbox-custom"></span>
              </label>
            </div>
            <div class="task-content">
              <div class="task-title">
                <input data-item-id="<?= $value['id'] ?>" type="text" disabled='disabled' name="title" value="<?= $value['title'] ?>" id="">
                <span class="form-validation"></span>
              </div>
              <div class="actions">
                <span class="icon-edit list-card-operation"></span>
                <span data-item-id="<?= $value['id'] ?>" class="delete-icon">
                </span>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
        <div class="task-item">
          <div class="task-content">
            <div class="w-100 text-right">
              <a id="show-more" class="show-more">Show more</a>
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php else : ?>
    <section class="content">
      <form class="form-signin text-center" action="/login/login" method="POST">
        <h1 class="h3 mb-3 font-weight-normal">Login form</h1>
        <div class="form-group">
          <label for="login" class="sr-only">login</label>
          <input type="text" id='login' class="form-control" placeholder="login" name='login' required="required" autofocus="">
          <?php if ($_SESSION['validationErrors']['login']) : ?>
            <span class="text-danger"><?= $_SESSION['validationErrors']['login']; ?></span>
          <?php endif; ?>
        </div>
        <div class="form-group">
          <label for="password" class="sr-only">Пароль</label>
          <input type="password" name="password" id="password" class="form-control" placeholder="Пароль" required="required">
          <?php if ($_SESSION['validationErrors']['password']) : ?>
            <span class="text-danger"><?= $_SESSION['validationErrors']['password']; ?></span>
          <?php endif; ?>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">login</button>
      </form>
    <?php endif; ?>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 id="modal-title" class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-header">
            <h6 id="modal-meta" class="modal-title" id="exampleModalLabel">
              </h5>
          </div>
          <div id="modal-text" class="modal-body">
            <label class="description-label" for="desc">
              Description
              <textarea disabled class="form-control" name="" id="desc" cols="30" rows="10"></textarea>
              <span class="form-validation"></span>
            </label>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" id="save" class="btn btn-success">Save</button>
          </div>
        </div>
      </div>
    </div>

    <?php if ($_SESSION['success']) : ?>
      <script>
        alert("Task successfully added!");
      </script>
      <?php unset($_SESSION['success']) ?>
    <?php endif; ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/app.js"></script>
</body>

</html>